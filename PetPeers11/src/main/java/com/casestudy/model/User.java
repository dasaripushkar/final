package com.casestudy.model;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
@Entity
@Table(name="user",uniqueConstraints = @UniqueConstraint(columnNames= {"userName"}))
public class User implements Serializable{
	@Id
	@Column(name="USERID")
	//@NotNull
	@GeneratedValue(strategy = GenerationType.AUTO)
	//@NotEmpty
	private Long userId;
	@Column(name="USERNAME")
	@NotNull
	@NotEmpty
	private String userName;
	@NotNull
	@Column(name="USERPASSWORD")
	@NotEmpty
	private String userPassword;
	private String confirmPassword;
	@OneToMany
	@JoinColumn(name = "PETOWNERID")
	Set<Pet> pets = new HashSet<Pet>();
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public Set<Pet> getPets() {
		return pets;
	}
	public void setPets(Set<Pet> pets) {
		this.pets = pets;
	}
	public User(Long userId, String userName, String userPassword, String confirmPassword, Set<Pet> pets) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userPassword = userPassword;
		this.confirmPassword = confirmPassword;
		this.pets = pets;
	}
	public User() {
		super();
		
	}
	
	
	
	

}
