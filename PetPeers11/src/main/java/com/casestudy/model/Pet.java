package com.casestudy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="pet")
public class Pet {


	@Id
	@Column(name="PETID")
	@NotNull
	@GeneratedValue(strategy = GenerationType.AUTO)
	//@NotEmpty
	private Long petId;
	@Column(name="PETNAME")
	@NotNull
	@NotEmpty
	private String petName;
	@NotNull
	@Column(name="PETAGE")
	@NotEmpty
	private Integer petAge;
	@NotNull
	@Column(name="PETPLACE")
	@NotEmpty
	private String petPlace;
	/*@NotNull
	@Column(name="PETOWNERID")
	@NotEmpty
	private int petOwnerId;*/
	@ManyToOne
	@JoinColumn(name="USERID_fk")
	private User user;





	public Long getPetId() {
		return petId;
	}
	public void setPetId(Long petId) {
		this.petId = petId;
	}
	public String getPetName() {
		return petName;
	}
	public void setPetName(String petName) {
		this.petName = petName;
	}
	public Integer getPetAge() {
		return petAge;
	}
	public void setPetAge(int petAge) {
		this.petAge = petAge;
	}
	public String getPetPlace() {
		return petPlace;
	}
	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}
	
	/*public int getPetOwnerId() {
		return petOwnerId;
	}
	public void setPetOwnerId(int petOwnerId) {
		this.petOwnerId = petOwnerId;
	}*/
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	

}
