package com.casestudy.validation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.exceptions.RegisterException;
import com.casestudy.model.User;
import com.casestudy.service.UserService;

@Component
public class UserValidator implements Validator {

	@Autowired
	UserService userService;

	public boolean supports(Class<?> arg0) {

		return false;
	}

	public void validate(Object arg0, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "Key1");

	}
	public boolean validateUserName(User user,Errors errors) throws RegisterException {
		User user1;
			user1 = userService.saveUser(user);
			if(user1!=null) {
				errors.rejectValue("userName", "msg2");
				return true;
				
			}
	
		return false;
		
		
	}
	

}
