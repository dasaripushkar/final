package com.casestudy.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casestudy.dao.UserLoginDao;
import com.casestudy.model.User;

@Service("UserLoginService")
@Transactional
public class UserLoginServiceImpl implements UserLoginService {
	@Autowired
	private UserLoginDao userLoginDao;

	User userLoginDao1 = null;

	@Override
	public User authenticateUser(String userName, String userPassword) {
		return userLoginDao1 = userLoginDao.authenticateUser(userName, userPassword);

	}
}