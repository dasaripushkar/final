package com.casestudy.service;

import com.casestudy.exceptions.RegisterException;
import com.casestudy.model.User;

public interface UserService {

	public abstract User saveUser(User user) throws RegisterException;

}
