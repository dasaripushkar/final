package com.casestudy.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casestudy.dao.UserDao;
import com.casestudy.exceptions.RegisterException;
import com.casestudy.model.User;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;

	public User saveUser(User user) throws RegisterException {
		User userDao1 = null;
		if (user.getUserPassword().equals(user.getConfirmPassword())) {
			userDao1 = userDao.saveUser(user);
			;
		} else {
			throw new RegisterException("Password does not match");
		}

		return userDao1;
	}

}
