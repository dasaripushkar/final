package com.casestudy.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.controller.SavePet;
import com.casestudy.dao.PetDao;
import com.casestudy.model.Pet;

@Service("petService")
@Transactional
public class petServiceImpl implements petService {
	private static final Logger logger = Logger.getLogger(petServiceImpl.class.getName());

	@Autowired
	private PetDao petDao1;

	@Override
	public Pet savePet(Pet pet) {
		logger.info(pet.getPetName());

		return petDao1.savePet(pet);

	}

	@Override
	public List<Pet> getAllPets() {
		
		return petDao1.getAllPets();
	}

}