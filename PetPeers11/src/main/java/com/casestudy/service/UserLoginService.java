package com.casestudy.service;

import com.casestudy.model.User;

public interface UserLoginService {

	public abstract User authenticateUser(String userName, String userPassword);
}
