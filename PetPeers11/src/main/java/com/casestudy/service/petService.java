package com.casestudy.service;

import java.util.List;

import com.casestudy.model.Pet;

public interface petService {

	public abstract Pet savePet(Pet pet);

	public List<Pet> getAllPets();

}
