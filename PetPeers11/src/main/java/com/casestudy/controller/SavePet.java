package com.casestudy.controller;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.dao.PetDaoImpl;
import com.casestudy.model.Pet;
import com.casestudy.service.petService;

@Controller
public class SavePet {
	private static final Logger logger = Logger.getLogger(SavePet.class.getName());

	@Autowired
	private petService petService1;

	@GetMapping(value = "addPets")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("addPets");
		Pet pet = new Pet();
		modelAndView.addObject("petModel", pet);
		return modelAndView;
	}

	@PostMapping(value = "savePet")
	public ModelAndView savePet(@ModelAttribute("petModel") Pet pet, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return new ModelAndView("homePage");
		}
		logger.info(pet.getPetName());

		petService1.savePet(pet);
		ModelAndView modelAndView = new ModelAndView("addPets");
		return modelAndView;
	}

}
