package com.casestudy.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.exceptions.RegisterException;
import com.casestudy.exceptions.userException;
import com.casestudy.model.User;
import com.casestudy.service.UserService;

@Controller
public class UserRegistration {

	@Autowired
	private UserService userService;

	@PostMapping(value = "register")
	public ModelAndView login(@ModelAttribute("userModel") User user) {
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		return modelAndView;
	}

	@RequestMapping(value = "loginsuccess")
	public ModelAndView saveUSer(@Valid @ModelAttribute("userModel") User user)
			throws RegisterException, userException {
		/*
		 * UserValidator userValidator = new UserValidator();
		 * userValidator.validateUserName(user, bindingResult);
		 */
		/*if (bindingResult.hasErrors()) {
			return new ModelAndView("registrationPage");
		}*/
		userService.saveUser(user);
		ModelAndView modelAndView = new ModelAndView("loginsuccess");
		return modelAndView;

	}

}
