package com.casestudy.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.servlet.ModelAndView;

public class UserLogout {

	@GetMapping(value = "logout")
	public ModelAndView login(HttpServletRequest request) {
		request.removeAttribute("User");
		ModelAndView modelAndView = new ModelAndView("login");

		return modelAndView;
	}

}