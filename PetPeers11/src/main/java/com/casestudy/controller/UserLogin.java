package com.casestudy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.servlet.ModelAndView;

import com.casestudy.model.User;
import com.casestudy.service.UserLoginService;
import com.casestudy.validation.LoginValidator;

@Controller
public class UserLogin {

	@Autowired
	private UserLoginService userLoginService;

	@GetMapping(value = "/")
	public ModelAndView init() {
		ModelAndView modelAndView = new ModelAndView("login");
		User user = new User();
		modelAndView.addObject("userModel", user);
		return modelAndView;
	}

	@PostMapping(value = "login")
	public ModelAndView login(@ModelAttribute("userModel") User user) {
		ModelAndView modelAndView = new ModelAndView("homePage");
		return modelAndView;
	}

	public ModelAndView authenticateUser(HttpServletRequest request, @ModelAttribute("user") User user,
			BindingResult bindingResult) {
		ModelAndView modelAndView = null;
		LoginValidator LoginValidation = new LoginValidator();
		LoginValidation.validate(user, bindingResult);
		if (bindingResult.hasErrors()) {
			return new ModelAndView("login");
		}

		User user1 = userLoginService.authenticateUser(user.getUserName(), user.getUserPassword());
		if (user1 != null) {
			ModelAndView modelAndView1 = new ModelAndView("homePage");
			return modelAndView1;
		} else {
			ModelAndView modelAndView1 = new ModelAndView("login");
		}
		return modelAndView;

	}

}
