package com.casestudy.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.model.Pet;
import com.casestudy.service.petService;

@Controller
public class home {

	@Autowired
	private petService petService1;

	private static final Logger logger = Logger.getLogger(home.class.getName());

	@PostMapping(value = "home")
	public ModelAndView init() {

		List<Pet> pet1 = petService1.getAllPets();
		for (Pet petList : pet1) {
			logger.info("pets:" + petList);

		}
		return null;
	}

}
