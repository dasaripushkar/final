package com.casestudy.dao;

import com.casestudy.model.User;

public interface UserLoginDao {

	public abstract User authenticateUser(String userName, String userPassword);

}
