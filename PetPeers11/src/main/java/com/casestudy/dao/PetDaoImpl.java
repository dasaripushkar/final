package com.casestudy.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.model.Pet;

@Repository
@Transactional
public class PetDaoImpl implements PetDao {
	@Autowired
	private SessionFactory sessionFactory;
	private static final Logger logger = Logger.getLogger(PetDaoImpl.class.getName());

	@Override
	public Pet savePet(Pet pet) {
		logger.info(pet.getPetName());
		try {
			pet.setPetId((long) 1);
			pet.setUser(null);
			logger.info(pet.getPetName());
			Session session = this.sessionFactory.getCurrentSession();
			logger.info(pet.getPetName());
			session.saveOrUpdate(pet);
			logger.info(pet.getPetName());
			session.flush();
			logger.info(pet.getPetName());
			return pet;
		} catch (Exception e) {
			e.getStackTrace();
		}
		return pet;
	}

	@Override
	public List<Pet> getAllPets() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Pet> petList = session.createQuery("from pet").list();
		return petList;
	}

}
