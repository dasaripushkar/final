package com.casestudy.dao;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.User;

@Transactional
@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User saveUser(User user) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(user);
			return user;
		} catch (Exception e) {
			e.getStackTrace();
		}
		return user;
	}

}
