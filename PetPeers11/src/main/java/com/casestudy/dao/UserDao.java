package com.casestudy.dao;

import com.casestudy.exceptions.RegisterException;
import com.casestudy.model.User;

public interface UserDao {
	public abstract User saveUser(User user) throws RegisterException;

}
