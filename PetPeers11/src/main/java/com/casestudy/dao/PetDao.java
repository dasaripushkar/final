package com.casestudy.dao;

import java.util.List;

import com.casestudy.model.Pet;

public interface PetDao {
	public abstract Pet savePet(Pet pet);

	public List<Pet> getAllPets();

}
