package com.casestudy.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import com.casestudy.model.User;

@Repository
@Transactional

public class UserLoginDaoImpl implements UserLoginDao {

	@Autowired
	private SessionFactory sessionFactory;
	User user = new User();

	private static final Logger logger = Logger.getLogger(UserLoginDaoImpl.class.getName());

	@Transactional
	public User authenticateUser(String username, String password) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			org.hibernate.Query query = session.createQuery(" select userPassword from User where userName=:userName");
			query.setParameter("userName", username);
			List<User> userPassword = query.list();
			if (userPassword.contains(password)) {
				return new User();
			}
		} catch (Exception e) {
			session.close();
			((Log) logger).error("Not found", e);

		}
		return null;
	}
}