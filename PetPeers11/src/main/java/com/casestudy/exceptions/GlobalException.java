package com.casestudy.exceptions;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalException {
	@ExceptionHandler(value= {RegisterException.class})
	    public ModelAndView processException(Exception exception) {
	        ModelAndView modelAndView=new ModelAndView("globalexceptionpage");
	        modelAndView.addObject("why", exception.getMessage());
	        return modelAndView;
	        
	  

	 

	}
	@ExceptionHandler(value= {LoginException.class})
    public ModelAndView processException1(Exception exception) {
        ModelAndView modelAndView=new ModelAndView("globalexceptionpage");
        modelAndView.addObject("why", exception.getMessage());
        return modelAndView;
  

}
}
