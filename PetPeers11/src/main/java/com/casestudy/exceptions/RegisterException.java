package com.casestudy.exceptions;

public class RegisterException extends Exception{
	
	private String message;

	public RegisterException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String getMessage() {
		
		return this.message;
	}

	

}
