<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<style>
li {
	display: inline;
	margin: 0px 0px 0px 20px;
}

div {
	height: 25px;
	background-color: grey
}
</style>
<meta charset="ISO-8859-1">
<title>Home Page</title>
</head>
<body>
	<h2 style="background-color: Black; color: White">PetShop</h2>
	<div>
		<ul>
			<li><a style="color: white" href="mypets">Home</a></li>
			<li><a style="color: white" href="myPets">MyPets</a></li>
			<li><a style="color: white" href="addPets">AddPets</a></li>
			<li><a style="float: right; color: white" href="logout">LogOut</a></li>
		</ul>
	</div>
</body>
</html>