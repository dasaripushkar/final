<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register Page</title>
</head>
<body>
	<h3 style="background-color: Black; color: white">PetShop</h3>
	<h2>Register</h2>

	<form:form action="loginsuccess" method="post"
		modelAttribute="userModel">
		<table>
			<tr>
				<td>Name</td>
				<td><form:input type="text" path="userName" size="50" /> <form:errors
						path="userName"></form:errors></td>
			</tr>

			<tr>
				<td>Password</td>
				<td><form:input type="password" path="userPassword"
						id="userPassword" size="50" /> <form:errors path="userPassword"></form:errors></td>
			</tr>

			<tr>
				<td>ConfirmPassword</td>
				<td><form:input type="password" path="confirmPassword"
						id="confirmPassword" size="50" /> <form:errors
						path="confirmPassword"></form:errors></td>
			</tr>
			<tr>
				<td><br />
				<input type="submit" value="Register"></td>
			</tr>
		</table>
	</form:form>


</body>
</html>