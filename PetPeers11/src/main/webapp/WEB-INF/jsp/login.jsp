<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
</head>
<body>
<h2 style="background-color:Black;color:White">PetShop</h2>
<form:form method="post" action="login" modelAttribute="userModel">
            
            <table>
            <tr>
                <td>Name</td>
                <td><form:input type="text" path="userName" size="50"/>
                         <form:errors path="userName"></form:errors></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><form:input type="password" path="userPassword" id="userPassword" size="50"/>
                         <form:errors path="userPassword"></form:errors></td>
            </tr>
           
            <tr>
                <td><br/><input type="submit"  value="Login" ></td>
            </tr>
            </table>
            </form:form>
            
           
            <form:form method="post" action="register" modelAttribute="userModel">
            <table>
            <tr>
                <td><br/><input type="submit"  value="New User Registration" ></td>
            </tr>
            </table>
            
             </form:form>
            
                         
            

</body>
</html>