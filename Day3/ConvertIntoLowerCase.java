package Day3;
// Convert String to a lower case
import java.util.Scanner;

public class ConvertIntoLowerCase 
{
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the String : ");
		// Taking String input
		String s = scanner.nextLine();
		//  Printing by converting to Lowercase
		System.out.println(s.toLowerCase());
		// dereferencing
		scanner.close();
	}
}
