package Day3;
// Convert the char of d to h
import java.util.Scanner;

public class ConvertdToh 
{
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the String");
		// Taking String input
		String s = scanner.nextLine();
		System.out.println(s.replace('d','h'));
		// dereferencing
		scanner.close();
		}
	}

