package Day3;

import java.util.Scanner;

public class PrintingSubStringFromGivenRange 
{
		public static void main(String[] args) 
		{
			Scanner scan = new Scanner(System.in);
			System.out.println("ENTER STRING");
			String s1 = scan.next();
			String s2 = "";
			System.out.println("ENTER START INDEX");
			int i = scan.nextInt();
			System.out.println("ENTER END INDEX");
			int j = scan.nextInt();
			if(j == s1.length())
			{
				s2 = s1.substring(i);
			}
			else
			{
				s2 = s1.substring(i,j);
			}
			System.out.println("ORIGINAL STRING : "+s1);
			System.out.println("SUB STRING : "+s2);
			scan.close();
		}
		
}
