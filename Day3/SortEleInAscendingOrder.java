package Day3;
// Arranging array in sorting order
import java.util.Arrays;

public class SortEleInAscendingOrder 
{
	public static void main(String[] args) 
	{
		int[] i = new int[5];
		i[0] = 5;
		i[1] = 4;
		i[2] = 3;
		i[3] = 2;
		i[4] = 1;
		Arrays.sort(i);
		for(int j : i)
		{
			System.out.println(j);
		}
	}
}
