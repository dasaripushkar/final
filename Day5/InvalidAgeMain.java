package Day5;
import java.util.Scanner;
public class InvalidAgeMain 
{
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws InvalidAgeException {
	Scanner scanner = new Scanner(System.in);
	System.out.println("Enter the player name: ");
	String name = scanner.next();
	System.out.println("Enter the player age: ");
	int age = scanner.nextInt();
	try {
	if (age < 19) {
	throw new InvalidAgeException();
	} else {
	System.out.println("Player name: " + name);
	System.out.println("Player age: " + age);
	}
	} catch (InvalidAgeException iae) {
	System.out.println("CustomException: InvalidAgeRangeException ");
	}

	scanner.close();
	}
	}



