package Day5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PrintingExceptionMessages 
{
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		
		try 
		{
			System.out.println("Enter the 1st input : ");
			int i = scanner.nextInt();
			System.out.println("Enter the 2nd input : ");
			int j = scanner.nextInt();
			int add = i+j;
			System.out.println(add);
			int sub = i-j;
			System.out.println(sub);
			int mul = i*j;
			System.out.println(mul);
			int div = i/j;
			System.out.println(div);
		}
		catch (InputMismatchException e) 
		{
			System.out.println("Error !! PLease enter integer datatype");
		}
		catch (ArithmeticException e) 
		{
			System.out.println("We cant divide by zero ");
		}
		scanner.close();
	}
	
}
