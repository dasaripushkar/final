package Day6;

import Day6.ThreadNameChange;

public class ThreadNameChangeMain {

	public static void main(String[] args) {
		ThreadNameChange thread = new ThreadNameChange();
		System.out.println("Thread Name: " + thread.getName());
		thread.setName("MyThread after name change");
		System.out.println("After change: " + thread.getName());
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
