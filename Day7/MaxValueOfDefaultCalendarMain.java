package Day7;

import java.util.Scanner;

import Day7.MaxValueOfDefaultCalendar;

public class MaxValueOfDefaultCalendarMain {
	public static void main(String[] args) {
		MaxValueOfDefaultCalendar noOfDays = new MaxValueOfDefaultCalendar();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the year: ");
		int year = scanner.nextInt();
		System.out.println("Enter the month: ");
		int month = scanner.nextInt();
		System.out.println("No of days: " + noOfDays.display(year, month));
		scanner.close();
	}

}
