package Day7;

import java.util.Calendar;

public class MaxValueOfYear {

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		System.out.println("\nCurrent Date and Time:" + cal.getTime());
		int MaxYear = cal.getActualMaximum(Calendar.YEAR);
		int MaxMonth = cal.getActualMaximum(Calendar.MONTH);
		int MaxWeek = cal.getActualMaximum(Calendar.WEEK_OF_YEAR);
		int MaxDate = cal.getActualMaximum(Calendar.DATE);

		System.out.println(" Maximum Year: " + MaxYear);
		System.out.println(" Maximum Month: " + MaxMonth);
		System.out.println(" Maximum Week of Year: " + MaxWeek);
		System.out.println(" Maximum Date: " + MaxDate + "\n");
	}

}
