package Day7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Date10 
{
	public static void findOldDate() throws ParseException
	{
		Date date = new Date();
		 SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
		 SimpleDateFormat sdformat1 = new SimpleDateFormat("MM/dd/yyyy");
	      Date d1 = sdformat.parse("05-12-1987");
	      Date d2 = sdformat.parse("08-11-2010");
	      System.out.println("The date 1 is: " + sdformat.format(d1));
	      System.out.println("The date 2 is: " + sdformat.format(d2));
	      if(d1.compareTo(d2) < 0) 
	      {
	    	  System.out.println("Date1 : " + sdformat1.format(d1));
	      }
	      else 
	      {
	    	  System.out.println("Date2 : " + sdformat1.format(d2));
	      }
	}
}

