package Day7;
import java.util.ArrayList;
import java.util.Collections;
public class ArrayListProg 
{
	public static void userMainCode() 
	{
		ArrayList<Integer> arraylist = new ArrayList<Integer>();
		arraylist.add(1);
		arraylist.add(2);
		arraylist.add(3);
		arraylist.add(4);
		arraylist.add(5);
		System.out.println("array list 1 : "+arraylist);
		ArrayList<Integer> arraylist2 = new ArrayList<Integer>();
		arraylist2.add(6);
		arraylist2.add(7);
		arraylist2.add(8);
		arraylist2.add(9);
		arraylist2.add(10);
		System.out.println("array list 2 : "+arraylist2);
		Collections.sort(arraylist);
		Collections.sort(arraylist2);
		arraylist.addAll(arraylist2);
		System.out.println(arraylist);
		System.out.println(arraylist.size());
		System.out.println(arraylist.get(2));
		System.out.println(arraylist.get(6));
		System.out.println(arraylist.get(8));
	}

}
