package Day7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormat 
{
	public static void dateFormat() throws ParseException
	{
		Date date = new Date();
		 SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyy");
		 SimpleDateFormat sdformat1 = new SimpleDateFormat("dd-MM-yyyy");
	     Date d1 = sdformat.parse("05/12/1987");
	     System.out.println("The date  is: " + sdformat1.format(d1));
	}
}
