package Day7;

import java.util.Scanner;

import Day7.CheckingCharacters;

public class CheckingCharactersMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the string: ");
		String string = scanner.nextLine();
		CheckingCharacters charCheck = new CheckingCharacters();
		int result = charCheck.checkCharacters(string);
		if (result == -1)
			System.out.println("Invalid Input");
		else if (result == 1)
			System.out.println("valid");
		else
			System.out.println("Invalid");
		scanner.close();

	}

}
