package Day7;


	import java.text.SimpleDateFormat;
	import java.util.Date;
	import java.util.Scanner;

	public class ExtractDateTime 
	{

	public static void main(String[] args) {
	String str = new String();
	Scanner scanner = new Scanner(System.in);
	System.out.println("Enter a String containing a date in yyyy-mm-dd HH:mm:ss format only");
	str = scanner.nextLine();

	try {

	Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
	String string = new SimpleDateFormat("dd/MM/yyyy H:mm:ss").format(date);
	System.out.println("The date is " + string + " (dd/mm/yyyy)(h:mm:ss) ");
	} catch (Exception e) {
	System.out.println("Exception occured " + e);
	}
	scanner.close();

	}

	
}
