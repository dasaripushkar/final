package Day7;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DifferenceInMonths {
	private static final String Months = null;

	public static void getMonthDifference() 
	{
		long daysBetween = ChronoUnit.MONTHS.between(LocalDate.parse("2011-03-01"),
			    LocalDate.parse("2012-04-16"));
		System.out.println(daysBetween);
	}
}
