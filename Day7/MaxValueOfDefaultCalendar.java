package Day7;

import java.util.Calendar;

public class MaxValueOfDefaultCalendar {
	public static int display(int year, int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		int DaysOfMonth = cal.getActualMaximum(cal.DAY_OF_MONTH);
		return DaysOfMonth;
	}

}
