package Day7;


	import java.util.ArrayList;
	import java.util.Scanner;

	import Day7.MergeSortedList;

	public class MergeSortedListMain 
	{

	public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	ArrayList<Integer> al1 = new ArrayList<Integer>();
	ArrayList<Integer> al2 = new ArrayList<Integer>();
	ArrayList<Integer> ans = new ArrayList<Integer>();
	for (int i = 0; i < 5; i++)
	al1.add(sc.nextInt());
	for (int j = 0; j < 5; j++)
	al2.add(sc.nextInt());
	ans = MergeSortedList.answer(al1, al2);
	for (int k = 0; k < 3; k++)
	System.out.println(ans.get(k));
	sc.close();

	}
}
