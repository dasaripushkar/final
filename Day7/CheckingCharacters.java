package Day7;

public class CheckingCharacters {
	public static int checkCharacters(String string) {
		int length = string.length();
		if (length < 2)
			return -1;
		if (string.charAt(0) == string.charAt(length - 1))
			return 1;
		else
			return 0;
	}

}
