package Day8;

import java.io.FileWriter;
import java.io.IOException;

public class WritePlainText {

	public static void main(String[] args) {
		try {
			FileWriter writer = new FileWriter("C:\\BufferedEXample\\Example2.txt", true);
			writer.write("Hello World");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
