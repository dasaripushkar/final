package Day8;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadingFirst3Lines {
	public static void main(String[] args) {

		String line = null;

		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader("C:\\BufferedEXample\\Example3.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		int i = 0;
		try {

			try {
				while (((line = bufferedReader.readLine()) != null) && i < 3) {
					System.out.println(line);
					i++;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} finally {
			try {
				bufferedReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
