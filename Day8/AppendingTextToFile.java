package Day8;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class AppendingTextToFile {
	public static void main(String[] args) {

		StringBuilder sb = new StringBuilder();
		String strLine = "";
		try {
			String filename = "C:\\BufferedEXample\\Example3.txt";
			FileWriter fw = new FileWriter(filename, true);
			// appends the string to the file
			fw.write("Hiii");
			fw.close();
			BufferedReader br = new BufferedReader(new FileReader("C:\\BufferedEXample\\Example3.txt"));
			// read the file content
			while (strLine != null) {
				sb.append(strLine);
				sb.append(System.lineSeparator());
				strLine = br.readLine();
			}
			System.out.println("done");
			br.close();
		} catch (IOException ioe) {
			System.err.println("IOException: " + ioe.getMessage());
		}
	}
}
