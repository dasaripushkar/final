package Day4;

public class MembershipCard3 extends Card3
{
	int rating;
	public MembershipCard3(String holderName, String cardNumber, String expiryDate,int rating)
	{
		super(holderName, cardNumber, expiryDate);
		this.rating = rating;
	}
	public int getRating() 
	{
		return rating;
	}
	public void setRating(int rating) 
	{
		this.rating = rating;
	}
	
}
