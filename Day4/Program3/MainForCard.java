package Day4;

import java.util.Scanner;

public class MainForCard 
{
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		MembershipCard3 membershipcard3 = new MembershipCard3("Pushkar","2145-0359","12/29",1);
		PayBackCard3 paybackCard3 = new PayBackCard3("Pushkar","2145-0359","12/29",100,1500.50);
		System.out.println("Enter to choose card");
		int i = scanner.nextInt();
		if(i == 1)
		{
			System.out.println("You choosen Member ship card");
			System.out.println("CARD HOLDER NAME : "+membershipcard3.holderName);
			System.out.println("CARD NUMBER IS : "+membershipcard3.cardNumber);
			System.out.println("CARD EXPIRY DATE : "+membershipcard3.expiryDate);
			System.out.println("RATING IS : "+membershipcard3.rating);
		}
		else
		{
			System.out.println("You choosen pay back card");
			System.out.println("CARD HOLDER NAME : "+paybackCard3.holderName);
			System.out.println("CARD NUMBER IS : "+paybackCard3.cardNumber);
			System.out.println("CARD EXPIRY DATE : "+paybackCard3.expiryDate);
			System.out.println("TOTAL POINTS EARNED ARE : "+paybackCard3.pointsEarned);
			System.out.println("TOTAL POINTS ARE : "+paybackCard3.totalAmount);
		}
		scanner.close();
	}
}
