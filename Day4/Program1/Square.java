package Day4;

public class Square extends Shape 
{	
	int side;

	public Square(String name, int side) {
		super(name);
		this.side = side;
		System.out.println(name);
	}

	@Override
	public float calculateArea() 
	{
		float area = (float) side*side;
		System.out.println("Area of square : "+area);
		return 0;
	}
	
}
