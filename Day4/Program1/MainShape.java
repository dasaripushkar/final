package Day4;

import java.util.Scanner;

public class MainShape 
{
	@SuppressWarnings("unused")
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		Circle circle = new Circle("Circle",3);
		String name1 = circle.name;
		Square square = new Square("Square",3);
		String name2 = square.name;
		Rectangle rectangle = new Rectangle("Rectangle",10,10);
		String name3 = rectangle.name;
		System.out.println("Enter the shape name : ");
		String name = scanner.next();
		if(name.equals(name1))
		{
			circle.calculateArea();
		}
		else if(name.equals(name2))
		{
			square.calculateArea();
		}
		else if(name.equals(name3))
		{
			rectangle.calculateArea();
		}
		else
		{
			System.out.println("not matched");
		}
		scanner.close();
	}
	
}
