package Day4;

public class Rectangle extends Shape 
{
	int length;
	int breadth;
	public Rectangle(String name, int length, int breadth) 
	{
		super(name);
		this.length = length;
		this.breadth = breadth;
		System.out.println(name);
	}
	@Override
	public float calculateArea() 
	{
		float area = (float) length*breadth;
		System.out.println("Area of rectangle : "+area);
		return 0;
	}
	
}
