package Day4;

public class Circle extends Shape
{
	int radius;
	public Circle(String name,int radius) {
		super(name);
		this.radius = radius;
		System.out.println(name);
	}
	public float calculateArea()
	{
		float area = (float) (3.14*radius*radius);
		System.out.println("Area of circle : "+area);
		return area;
	}
}
