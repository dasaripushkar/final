package Day4;

public class Main5 
{
	    public static void main(String[] args) 
	    { 
	        FirstClass5 objA = new FirstClass5(); 
	        SecondClass5 objB = new SecondClass5(); 
	        System.out.println("in main(): "); 
	        System.out.println("objA.a = "+objA.getFirstClass()); 
	        System.out.println("objB.b = "+objB.getSecondClass()); 
	        objA.setFirstClass (222); 
	        objB.setSecondClass (333.33); 
	        System.out.println("objA.a = "+objA.getFirstClass()); 
	        System.out.println("objB.b = "+objB.getSecondClass()); 
	    } 
} 

	 

