package Day4;

public class FirstClass5 {
	int a = 100;

	public void FirstClass () 
	    { 
	        System.out.println("in the constructor of class FirstClass: "); 
	        System.out.println("a = "+a); 
	        a = 333; 
	        System.out.println("a = "+a); 

	    }

	public void setFirstClass(int value) 
	{

		a = value;

	}

	public int getFirstClass() 
	{

		return a;

	}
}
