package Day4;

public class Main4 
{
	public static void main(String[] args) { 
        SolvingError4 error4 = new SolvingError4();
        System.out.println("in main(): "); 
        // Getting the value before initiating
        System.out.println("objA.a = "+error4.getA()); 
        error4.setA(222);
        // Getting value after initiating
        System.out.println("objA.a = "+error4.getA());
    } 
}
