package Day2;

import java.util.Scanner;

public class SmallestOfThreeNumbers 
{
	public static void main(String[] args) 
	{
		int number1;
		int number2;
		int number3;
		Scanner scanner = new Scanner(System.in);
		System.out.println("enter first number");
		number1 = scanner.nextInt();
		System.out.println("enter second number");
		number2 = scanner.nextInt();
		System.out.println("enter third number");
		number3 = scanner.nextInt();
		if(number1 > number2 && number1 > number3)
		{
			System.out.println(number1+" is greatest");
		}
		else if(number2 > number3 && number2 > number1)
		{
			System.out.println(number2+" is greatest");
		}
		else
		{
			System.out.println(number3+" is greatest");
		}
	}
}
