package Day2;
import java.util.Scanner;
public class VowelsCount 
{
	public static void main(String[] args) 
	{
		String s;
		int count=0;
		Scanner scanner = new Scanner(System.in);
		System.out.println("enter a string");
		s = scanner.nextLine();
		for(int i=0;i<=s.length()-1;i++)
		{
			if(s.charAt(i)=='a' || s.charAt(i)=='e' || s.charAt(i)=='i' || s.charAt(i)=='o' || s.charAt(i)=='u')
			{
				count++;
			}
		}
		System.out.println("COUNT OF VOWELS IN GIVEN STRING "+s+" IS "+count);
	}
}
