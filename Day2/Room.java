package Day2;

public class Room 
{
	int roomNumber;
	String roomType;
	float roomArea;
	boolean ACmachine;
	public int getRoomNumber() 
	{
		return roomNumber;
	}
	public void setRoomNumber(int roomNumber) 
	{
		this.roomNumber = roomNumber;
	}
	public String getRoomType() 
	{
		return roomType;
	}
	public void setRoomType(String roomType) 
	{
		this.roomType = roomType;
	}
	public float getRoomArea() 
	{
		return roomArea;
	}
	public void setRoomArea(float roomArea) 
	{
		this.roomArea = roomArea;
	}
	public boolean isACmachine() 
	{
		return ACmachine;
	}
	public void setACmachine(boolean aCmachine) 
	{
		ACmachine = aCmachine;
	}
	
}
