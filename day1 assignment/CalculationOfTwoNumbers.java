
public class CalculationOfTwoNumbers 
{
	public static void main(String[] args) 
	{
		int firstNumber = 20;
		int secondNumber = 4;
		System.out.println("Addition of two numbers is : "+(firstNumber+secondNumber));
		System.out.println("Subtraction of two numbers is : "+(firstNumber-secondNumber));
		System.out.println("Multiplication of two numbers is : "+(firstNumber*secondNumber));
		System.out.println("Division of two numbers is : "+(firstNumber/secondNumber));
		System.out.println("Remainder of two numbers is : "+(firstNumber%secondNumber));
	}
}
