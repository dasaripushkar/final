
public class SwappingOfTwoNumbers 
{
	public static void main(String[] args) 
	{
		int first = 1;
		int second = 2;
		int temporary = first;
		first = second;
		second = temporary;
		System.out.println(first);
		System.out.println(second);
	}
}
