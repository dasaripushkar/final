
public class LongestSubString 
{
	public static void main(String[] args) 
	{
		String s1 = "Welcome to my HCL";
		String[] s2 = s1.split(" ");
		String longestSubString = s2[0];
		int longestSubStringLen = s2[0].length();
		for(int i=1;i<=s2.length-1;i++)
		{
			if(s2[i].length() > longestSubStringLen)
			{
				longestSubString = s2[i];
				longestSubStringLen = s2[i].length();
			}
		}
		System.out.println(longestSubString);
		System.out.println(longestSubStringLen);
	}
}
