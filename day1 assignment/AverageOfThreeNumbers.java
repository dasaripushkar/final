
public class AverageOfThreeNumbers 
{
	public static void main(String[] args) 
	{
		int firstNumber = 10;
		int secondNumber = 20;
		int thirdNumber = 30;
		int sum = firstNumber+secondNumber+thirdNumber;
		float average = sum/3;
		System.out.println("Average is : "+average);
	}
}
